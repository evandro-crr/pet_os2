#!/bin/bash

set -e -u

sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/#\(pt_BR\.*\)/\1/' /etc/locale.gen
locale-gen

mv /root/mirrorlist /etc/pacman.d/

ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

systemctl enable pacman-init.service choose-mirror.service
systemctl set-default graphical.target

rm etc/systemd/system/getty@tty1.service.d/autologin.conf

sed -i "s/#\(auto_login \).\+/\1yes/" /etc/slim.conf
sed -i "s/#\(default_user \).\+/\1seccom/" /etc/slim.conf

systemctl enable slim
systemctl enable dhcpcd
systemctl enable NetworkManager

pacman-key --init
pacman-key --populate archlinux
pacman -Syu --noconfirm

cp /root/wallpaper.png /usr/share/backgrounds/gnome/adwaita-day.jpg 
cp /root/wallpaper.png /usr/share/backgrounds/gnome/adwaita-morning.jpg 
mv /root/wallpaper.png /usr/share/backgrounds/gnome/adwaita-night.jpg 

pip install poetry
pip install QSystem==1.2.0b1

useradd -m -G lock,uucp,docker -s /usr/bin/zsh seccom

systemctl enable docker

chown -R seccom:seccom /home/seccom/
chmod +x /home/seccom/Desktop/*

sed -i "s/# \(ALL ALL=(ALL) \).\+/\1NOPASSWD: ALL/" /etc/sudoers

su -c "/home/seccom/customize_ainorootfs.sh" -l seccom

sed -i "s/\(ALL ALL=(ALL).\+\)/\# \1/" /etc/sudoers
rm -f /home/seccom/customize_ainorootfs.sh
rm -f /home/seccom/packages.aur

pacman -R sudo --noconfirm

passwd -l root

