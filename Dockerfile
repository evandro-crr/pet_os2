# sudo docker build -t stark/pet_os2 .
# sudo docker run --rm --privileged=true -v /home/stark/Documentos/petos/pet_os2/:/pet_os2/ stark/pet_os2
FROM archlinux/base

MAINTAINER Evandro Chagas Ribeiro da Rosa

RUN pacman -Syu archiso base-devel --noconfirm

WORKDIR /pet_os2
CMD ./makeiso.sh
