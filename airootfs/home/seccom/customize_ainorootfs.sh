#!/bin/zsh

######### trizen ########
cd /home/seccom/; wget https://aur.archlinux.org/cgit/aur.git/snapshot/trizen.tar.gz

tar xvf /home/seccom/trizen.tar.gz
rm /home/seccom/*.tar.gz

cd /home/seccom/trizen; makepkg; sudo pacman -U *.pkg.tar.xz --noconfirm
rm -r /home/seccom/trizen/
#########################

######### aur ###########
while read aur; do 
  trizen -S $aur --noconfirm
done < /home/seccom/packages.aur
#########################

date -R > ~/.build
gsettings set org.cinnamon.desktop.screensaver lock-enabled false
